import { UserLayout } from '~/layouts';
import { MapAnnouncement } from '~/components';
import { trpc } from '~/utils';

export default function TransactionHistory() {
  const { data } = trpc.announcement.getAnnouncement.useQuery();

  if (!data || !data.announcements) return null;

  const { announcements } = data;

  return (
    <UserLayout>
      <div className="card bg-base-100 shadow-xl">
        <div className="card-body">
          <h2 className="card-title">Transaction History</h2>
          <MapAnnouncement announcements={announcements} />
        </div>
      </div>
    </UserLayout>
  );
}
